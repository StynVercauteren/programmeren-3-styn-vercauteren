package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;

public class ServerStub implements Server {
    private final MessageManager messageManager;
    private final NetworkAddress serverAdress;
    private final NetworkAddress skeletonAdress;

    public ServerStub(NetworkAddress anAddress, NetworkAddress anotherAddress) {
        this.messageManager = new MessageManager();
        this.serverAdress = anAddress;
        this.skeletonAdress = anotherAddress;
    }

    @Override
    public void convert(MyDocument document) {
        MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "convert");
        methodCallMessage.setParameter("documentSkeletonAddress", skeletonAdress.toString());

        messageManager.send(methodCallMessage, serverAdress);

        MethodCallMessage reply = messageManager.wReceive();
        if (!reply.getMethodName().equals("convertResult")) {
            throw new RuntimeException("Expected convertResult, got: " + reply.getMethodName());
        }
    }
}
