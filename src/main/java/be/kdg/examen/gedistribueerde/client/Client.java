package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.server.Server;

public class Client {
    private final Server server;
    private final MyDocument document;

    public Client(Server server, MyDocument document) {
        this.server = server;
        this.document = document;
    }

    public void run() {
        String tekst = "Dit is een TESTje";
        document.setText(tekst);
        server.convert(document);
        System.out.println("myDocument.getText() = " + document.getText());
    }
}
