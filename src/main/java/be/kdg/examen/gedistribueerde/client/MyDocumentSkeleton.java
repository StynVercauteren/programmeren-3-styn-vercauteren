package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class MyDocumentSkeleton implements Runnable {
    private final MessageManager messageManager;
    private MyDocumentImpl document;

    public MyDocumentSkeleton(MyDocumentImpl myDocument) {
        messageManager = new MessageManager();
        this.document = myDocument;
    }

    public NetworkAddress getAddress() {
        return messageManager.getMyAddress();
    }

    @Override
    public void run() {
        while (true) {
            MethodCallMessage msg = messageManager.wReceive();

            if (msg.getMethodName().equals("getText")) {
                MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "getTextReply");
                reply.setParameter("text", document.getText());

                sendReply(reply, msg.getOriginator());
            } else if (msg.getMethodName().equals("setText")) {
                document.setText(msg.getParameter("text"));

                sendReply(new MethodCallMessage(messageManager.getMyAddress(), "setTextReply"), msg.getOriginator());
            }
        }
    }

    private void sendReply(MethodCallMessage reply, NetworkAddress originator) {
        messageManager.send(reply, originator);
    }
}
