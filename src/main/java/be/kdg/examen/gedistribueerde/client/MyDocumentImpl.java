package be.kdg.examen.gedistribueerde.client;

public class MyDocumentImpl implements MyDocument {
    private String text;

    public MyDocumentImpl() {
        this.text = "";
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }
}
