package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.MyDocument;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class ServerSkeleton {
    private final Server myServer;
    private final MessageManager messageManager;

    public ServerSkeleton(Server server) {
        myServer = server;
        messageManager = new MessageManager();
    }

    public void run() {

        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }

    private void handleRequest(MethodCallMessage request) {
        System.out.println("Received message with method " + request.getMethodName());

        if (request.getMethodName().equals("convert")) {
            handleConvert(request);
        }
    }

    private void handleConvert(MethodCallMessage request) {
        String documentSkeletonAddress = request.getParameter("documentSkeletonAddress");
        String documentSkeletonIP = documentSkeletonAddress.split(":")[0];
        int documentSkeletonPort = Integer.parseInt(documentSkeletonAddress.split(":")[1]);

        MyDocument myDocument = new MyDocumentStub(new NetworkAddress(documentSkeletonIP, documentSkeletonPort));

        this.myServer.convert(myDocument);
        System.out.println("Document converted: " + myDocument.getText());

        sendReply(request);
    }

    private void sendReply(MethodCallMessage request) {
        messageManager.send(new MethodCallMessage(messageManager.getMyAddress(), "convertResult"), request.getOriginator());
        System.out.println("reply sent to: " + request.getOriginator());
    }

    public NetworkAddress getAddress() {
        return messageManager.getMyAddress();
    }
}
