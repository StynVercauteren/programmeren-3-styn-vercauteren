package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.MyDocument;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class MyDocumentStub implements MyDocument {
    private final MessageManager messageManager;
    private final NetworkAddress address;

    public MyDocumentStub(NetworkAddress anAddress) {
        this.messageManager = new MessageManager();
        this.address = anAddress;
    }

    @Override
    public String getText() {
        MethodCallMessage getText = new MethodCallMessage(
                messageManager.getMyAddress(),
                "getText"
        );

        messageManager.send(getText, address);

        MethodCallMessage reply = messageManager.wReceive();
        if (!reply.getMethodName().equals("getTextReply")) {
            throw new RuntimeException("Expected getTextReply, got " + reply.getMethodName());
        }

        return reply.getParameter("text");
    }

    @Override
    public void setText(String text) {
        MethodCallMessage setText = new MethodCallMessage(
                messageManager.getMyAddress(),
                "setText"
        );

        setText.setParameter("text", text);

        messageManager.send(setText, address);

        MethodCallMessage reply = messageManager.wReceive();
        if (!reply.getMethodName().equals("setTextReply")) {
            throw new RuntimeException("Expected setTextReply, got " + reply.getMethodName());
        }
    }
}
