package be.kdg.examen.reflection;

public final class ExpectedAction {
    private final String methodName;
    private final String parameterValue;
    private final String returnValue;

    public ExpectedAction(String methodName, String parameterValue, String returnValue) {
        this.methodName = methodName;
        this.parameterValue = parameterValue;
        this.returnValue = returnValue;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public String getReturnValue() {
        return returnValue;
    }
}
