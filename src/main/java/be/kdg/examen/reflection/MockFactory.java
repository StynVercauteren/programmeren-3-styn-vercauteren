package be.kdg.examen.reflection;

import java.lang.reflect.Proxy;
import java.util.List;

public class MockFactory {
    // aan te vullen!
    public static Object create(Class itf, List<ExpectedAction> expectedActions) {
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{itf}, new MyInvocationHandler(expectedActions));
    }
}
