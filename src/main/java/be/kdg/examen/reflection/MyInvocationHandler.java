package be.kdg.examen.reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;

public class MyInvocationHandler implements InvocationHandler {
    private final List<ExpectedAction> myExpectedActions;

    public MyInvocationHandler(List<ExpectedAction> expectedActions) {
        this.myExpectedActions = expectedActions;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        if (args != null && args.length > 1) {
            throw new WrongActionException("Maximum amount of parameters is 1");
        }

        ExpectedAction action = findAction(method.getName(), args == null ? null : args[0]);

        try {
            return Integer.parseInt(action.getReturnValue());
        } catch (NumberFormatException e) {
            if (action.getReturnValue().length() == 1) {
                return action.getReturnValue().charAt(0);
            }
            return action.getReturnValue();
        }
    }

    private ExpectedAction findAction(String name, Object arg) {
        if (arg == null) {
            return myExpectedActions.stream()
                    .filter(a -> a.getMethodName().equals(name) && a.getParameterValue().equals("null"))
                    .findFirst()
                    .orElseThrow(() -> new WrongActionException("Action " + name + " with null parameter not found"));
        } else {
            return myExpectedActions.stream()
                    .filter(a -> a.getMethodName().equals(name) && a.getParameterValue().equals(arg.toString()))
                    .findFirst()
                    .orElseThrow(() -> new WrongActionException("Action " + name + " with parameter " + arg + " not found"));
        }
    }
}
