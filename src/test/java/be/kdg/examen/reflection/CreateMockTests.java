package be.kdg.examen.reflection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class CreateMockTests {
    private MyTestInterface myInterface;

    @BeforeEach
    public void setup() {
        List<ExpectedAction> expectedActions = new ArrayList<>();
        expectedActions.add(new ExpectedAction("doeA", "5", "null"));
        expectedActions.add(new ExpectedAction("doeA", "42", "null"));
        expectedActions.add(new ExpectedAction("getA", "null", "5"));
        expectedActions.add(new ExpectedAction("doeB", "Bla", "null"));
        expectedActions.add(new ExpectedAction("doeC", "null", "null"));
        expectedActions.add(new ExpectedAction("getC", "null", "A"));
        expectedActions.add(new ExpectedAction("getB", "5", "Foo"));

        myInterface = (MyTestInterface) MockFactory.create(MyTestInterface.class, expectedActions);
    }

    @Test
    void testCallingMethodDoeA() {
        Assertions.assertDoesNotThrow(() -> myInterface.doeA(5));
        Assertions.assertDoesNotThrow(() -> myInterface.doeA(42));
    }

    @Test
    void testCallingMethodGetA() {
        Assertions.assertEquals(myInterface.getA(), 5);
    }

    @Test
    void testCallingMethodDoeB() {
        Assertions.assertDoesNotThrow(() -> myInterface.doeB("Bla"));
    }

    @Test
    void testCallingMethodDoeC() {
        Assertions.assertDoesNotThrow(() -> myInterface.doeC());
    }

    @Test
    void testCallingMethodGetC() {
        Assertions.assertEquals(myInterface.getC(), 'A');
    }

    @Test
    void testCallingMethodGetB() {
        Assertions.assertEquals(myInterface.getB(5), "Foo");
    }

    @Test
    void testCallingOtherMethodThanExpectedWithNullParameter() {
        try {
            Assertions.assertEquals(myInterface.getD(), "Foo");
            Assertions.fail();
        } catch (WrongActionException e) {
            Assertions.assertEquals(e.getMessage(), "Action getD with null parameter not found");
        }
    }

    @Test
    void testCallingOtherMethodThanExpectedWithParameter() {
        try {
            Assertions.assertEquals(myInterface.getE(1), "Foo");
            Assertions.fail();
        } catch (WrongActionException e) {
            Assertions.assertEquals(e.getMessage(), "Action getE with parameter 1 not found");
        }
    }

    @Test
    void testCallingMethodGetBWithWrongParameter() {
        try {
            Assertions.assertEquals(myInterface.getB(10), "Foo");
            Assertions.fail();
        } catch (WrongActionException e) {
            Assertions.assertEquals(e.getMessage(), "Action getB with parameter 10 not found");
        }
    }

    @Test
    void testCallingMethodWithMoreThanOneParameter() {
        try {
            Assertions.assertEquals(myInterface.doeD(1, 2, 3), "Foo");
            Assertions.fail();
        } catch (WrongActionException e) {
            Assertions.assertEquals(e.getMessage(), "Maximum amount of parameters is 1");
        }
    }
}
